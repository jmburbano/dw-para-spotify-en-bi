from os import environ
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import pandas as pd
import time
import sys
import json
from tabulate import tabulate
from sqlalchemy import create_engine

##metodo para acceder a la api de spotify, credenciales otorgadas como variables del sistema
def authenticate(cliend_id: str, client_secret: str) -> spotipy.client.Spotify:
    sp = spotipy.Spotify(
        client_credentials_manager=SpotifyClientCredentials(
            client_id=cliend_id,
            client_secret=client_secret
        )
    )

    return sp

##metodo para consumir de la api de spotify, los artistas segun el criterio de seleccion
def getArtista (sp: spotipy.client.Spotify, artista: str):
    if len(sys.argv) > 1:
        name = ' '.join(sys.argv[1:])
    else:
        name = artista
    results = sp.search(q='artist:' + name, type='artist')
    items = results['artists']['items']
    if len(items) > 0:
        artist = items[0]
        return artist

##metodo para llenar la tabla o dataframe con los artistas y atributos seleccionados
def llenarArtistas(dataframe:pd.DataFrame,artistas:[str],sp: spotipy.client.Spotify):
    index=0
    for artista in artistas:
        art=getArtista(sp,artista)
        dataframe.loc[index]=[art['name'], art['popularity'], art['type'],art['external_urls']['spotify'], art['followers']['total'], art['href'], time.time() , "Api Spotify" ]
        index+=1
    return
##metodo para llenar la tabla o dataframe con las canciones y atributos seleccionados
def llenarCanciones(dataframeCanciones:pd.DataFrame,dataframeArtistas:pd.DataFrame,sp: spotipy.client.Spotify):
    index: int=0
    for i in dataframeArtistas.index:
        results=sp.artist_top_tracks(dataframeArtistas['Url'][i])
        for iterador in range(10):
            dataframeCanciones.loc[index] = [results['tracks'][iterador]['name'], results['tracks'][iterador]['type'], results['tracks'][iterador]['artists'][0]['name'],
                                     results['tracks'][iterador]['album']['name'], results['tracks'][iterador]['track_number'],
                                     results['tracks'][iterador]['popularity'], results['tracks'][iterador]['id'],
                                     results['tracks'][iterador]['external_urls']['spotify'], results['tracks'][iterador]['album']['release_date'],
                                     getArtista(sp,(results['tracks'][iterador]['artists'][0]['name']))["genres"], time.time(), "Api Spotify"]
            index=index+1
    return

if __name__ == "__main__":
    # trae las credenciales como variables del sistem
    CLIENT_ID = environ.get("SPOTIFY_CLIENT_ID")
    CLIENT_SECRET = environ.get("SPOTIFY_CLIENT_SECRET")
    # consigue autenticar y conectar con la api de spotify
    sp_instance = authenticate(CLIENT_ID, CLIENT_SECRET)
    #llena la tabla artistas con los criterios seleccionados
    artist_column_names = ["Nombre", "Popularaidad", "Tipo", "Url", "Seguidores", "Href", "Fecha_Carga", "Origen"]
    artistasDF = pd.DataFrame(columns=artist_column_names)
    artistas=["Queen","Metallica","Black Sabbath","Led Zeppelin","Iron Maiden","Nirvana","The Doors","Heroes Del Silencio","Soda Stereo","Megadeth"]
    llenarArtistas(artistasDF,artistas,sp_instance)
    #llena la tabla canciones con los criterios elegidos
    canciones_column_names=["Nombre","Tipo","Artista","Album","# Cancion","Popularidad","Id","Url","Fecha Lanzamiento","Genero","Fecha_Carga","Origen"]
    cancionesDF=pd.DataFrame(columns=canciones_column_names)
    llenarCanciones(cancionesDF,artistasDF,sp_instance)
    print(tabulate(artistasDF, headers='keys', tablefmt='psql'))
    print(tabulate(cancionesDF, headers='keys', tablefmt='psql'))
    #conecta con el motor postgres y carga las tablas
    engine = create_engine('postgresql://postgres:america13@127.0.0.1:5432/DWspotify')
    artistasDF.to_sql('Artistas', con=engine, index=False, if_exists='replace')
    cancionesDF.to_sql('Canciones', con=engine, index=False, if_exists='replace')
